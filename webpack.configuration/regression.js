const pkg = require('../package');
const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const AppCachePlugin = require('appcache-webpack-plugin');

console.log('Environment: REGRESSION');

module.exports = {
    context: __dirname,
    port: 8080,
    devtool: '#source-map',
    entry: {
        app: [
            'whatwg-fetch',
            'babel-polyfill',
            '../app/index.js'
        ],
        env: '../app/env/env-regression.js'
    },
    output: {
        path: path.join(__dirname, '../dist/core'),
        filename: '[name].js',
        publicPath: '/core/',
        library: (['Environment', '[name]']),
        libraryTarget: 'umd'
    },
    resolve: {
        extensions: ['', '.scss', '.js', '.json'],
        packageMains: ['browser', 'web', 'browserify', 'main', 'style']
    },
    module: {
      loaders: [
          {
              test: /\.js$/,
              loader: 'babel!strip-loader?strip[]=console.debug',
              exclude: [/(node_modules)/, /react-css-themr/]
          },
          {
              test: /\.json$/,
              loader: 'json',
              exclude: [/(node_modules)/, /react-css-themr/]
          },
          {
              test: /\.(scss|css)$/,
              exclude: 'app/common',
              loader: ExtractTextPlugin.extract('style', 'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap')
          },
          {
              test: /.*\.(gif|png|jpe?g|svg)$/i,
              loader: 'file-loader?name=[sha512:hash:base64:7].[ext]'
          }
        ]
    },
    sassLoader: {
        data: '@import "' + path.resolve(__dirname, '../app/common/theme/default.scss') + '";'
    },
    imageWebpackLoader: {
        pngquant: {
            quality: '65-90',
            speed: 4
        },
        svgo: {
            plugins: [
              {
                  removeViewBox: false
              },
              {
                  removeEmptyAttrs: false
              }
            ]
        }
    },
    postcss: [autoprefixer],
    plugins: [
        new ExtractTextPlugin('app.css', { allChunks: true }),
        new webpack.NoErrorsPlugin(),

        new webpack.optimize.OccurenceOrderPlugin(true),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
          output: {
              comments: false
          },
          compress: {
              warnings: false,
              screw_ie8: true
          }
      }),

      new webpack.DefinePlugin({
          'process.env': {
              NODE_ENV: JSON.stringify('production')
          },
          VERSION: JSON.stringify(pkg.version),
          BUILD_DATE: JSON.stringify((new Date()).toUTCString()),
          BUILD_NUMBER: JSON.stringify(process.env.BUILD_NUMBER)
      }),
      new AppCachePlugin({
          network: ['*'],
          exclude: [/.*\.js$/, /.*\.css$/, /.*\.map$/, /.*\.png$/, /.*\.html$/],
          output: 'hcp.appcache'
      })
    ]
};
