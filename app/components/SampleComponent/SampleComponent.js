import React from 'react';
import OtherComponent from './SampleComponentTwo.js';
class SampleComponent extends React.Component {

  static contextTypes = {
      params: React.PropTypes.object
  }

  constructor () {
      super();
  }

  state = {}

  render () {
      return <div>
        sample component #1 with param id = {this.context.params.id || '???'}
        <OtherComponent />
      </div>;
  }
}

export default SampleComponent;
