import React from 'react';
class SampleComponent extends React.Component {

  static contextTypes = {
      params: React.PropTypes.object
  }

  constructor () {
      super();
  }

  state = {}

  render () {
      return <div>
        sample component #2 with param id = {this.context.params.id || '???'}
      </div>;
  }
}

export default SampleComponent;
