import React from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
import { error } from '../../reducers';
import { Router, Route, IndexRoute, Redirect } from 'react-router';

// Bootstrap
import App from '../../views/App.js';

// Views
import Home from '../../views/Home/Home.js';
import MaterialDemo from '../../views/MaterialDemo/MaterialDemo.js';
import PpvDemo from '../../views/PpvDemo/PpvDemo.js';

// Add the reducer to your store on the `routing` key
const store = createStore(
    combineReducers({
        error,
        routing: routerReducer
    })
);

export const dispatch = store.dispatch;

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);

class RootComponent extends React.Component {

  constructor () {
      super();
  }

  state = {}

  render () {
      return <Provider store={store}>
        <Router history={history}>
          <Route path="/" component={App}>
            <IndexRoute component={Home} />
            <Route path="/ppv-demo" component={PpvDemo} />
            <Route path="/material-design" component={MaterialDemo} />
            <Route path="/material-design-:id" component={MaterialDemo} />
          </Route>
          <Redirect from="*" to="/" />
        </Router>
      </Provider>;
  }
}

export default RootComponent;
