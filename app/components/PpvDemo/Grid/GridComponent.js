import React from 'react';
import { StickyContainer, Sticky } from 'react-sticky';
import Row from '../Row/RowComponent.js';
import HeaderRow from '../HeaderRow/HeaderRowComponent.js';
import SummaryRow from '../SummaryRow/SummaryRowComponent.js';
import {Utils} from '../../../utils/utils.js';
import style from './GridComponent.scss';
class Grid extends React.Component {

  static contextTypes = {}

  constructor (props) {
      super();
      //TODO: this is just a stub data
      this.sumRow = {PoNumber: 'N/A', ActualUnitPrice: 'AVG 27.72', SomeOtherPriceCoef: 'AVG 48.85', CompletelyMadeUpNumber: 17, ActualPriceCoef: 31.5, RandomComparator: 'Random', Ppv: 12.33, VatIncluded: 'True/False', OrbitCost: 43.3, TwrParameter: 'Parameter 1 & 2', MinionOperator: 'max A(1)', KyeberCrystalValue: 'AVG 3'};

  }
  resetSort = () => {
      this.refs.header.resetSort();
  }
  updateSummary = () => {
      if (this.props.viewData.length > 0) {
          const columns = Object.keys(this.props.viewData[0]);
          return Utils.reduceObjectProperties(this.sumRow, columns);
      }
      else {
          return {};
      }
  }
  componentDidMount = () => {
      var scrollingDiv = document.getElementById('mygrid');
      scrollingDiv.addEventListener('touchmove', function(event){
          event.stopPropagation();
      });
  }
  render () {
      const items = this.props.viewData.map((item, index) => {
          let key = item.PoNumber;
          if (!item.PoNumber) {
              key = index;
          }
          return <Row item={item} key={key} index={index} />;
      });
      return (
              <div className={style.Grid} id="mygrid">
                  <HeaderRow item={this.props.viewData[0]} handleSortChange={this.props.handleSortChange} ref="header"/>
                  <div className={style.rows}>
                    {
                      (() => {
                        if (items.length < 1) {
                          return <p className={style.empty}>The list is empty</p>;
                        } else {
                          return items;
                        }
                      })()
                    }
                    <SummaryRow item={this.updateSummary()} />
                </div>
              </div>
      );
  }

}

Grid.propTypes = {
    handleSortChange: React.PropTypes.func,
    viewData: React.PropTypes.array
};
export default Grid;
