import React from 'react';
import {Tabs, Tab} from 'react-toolbox';
import {Utils} from '../../../utils/utils.js';
import CustomMenu from '../CustomMenu/CustomMenuComponent.js';
import Grid from '../Grid/GridComponent.js';
import style from './GridViewComponent.scss';

class GridViewComponent extends React.Component {

   constructor (props) {
      super(props);
      this.data = [
          {PoNumber: '23222115229', ActualUnitPrice: 36, SomeOtherPriceCoef: 76, CompletelyMadeUpNumber: 7, ActualPriceCoef: 30, RandomComparator: 'Random', Ppv: 12.3, VatIncluded: 'True', OrbitCost: 43.3, TwrParameter: 'Parameter 1', MinionOperator: 'A(1)', KyeberCrystalValue: 3},
          {PoNumber: '54982948492', ActualUnitPrice: 93, SomeOtherPriceCoef: 117, CompletelyMadeUpNumber: 9, ActualPriceCoef: 91, RandomComparator: 'Cargo', Ppv: 32.6, VatIncluded: 'False', OrbitCost: 78.55, TwrParameter: 'Parameter 2', MinionOperator: 'FA(1)', KyeberCrystalValue: 1},
          {PoNumber: '84383215229', ActualUnitPrice: 26.5, SomeOtherPriceCoef: 66, CompletelyMadeUpNumber: 17, ActualPriceCoef: 20, RandomComparator: 'Default', Ppv: 2.3, VatIncluded: 'False', OrbitCost: 43.3, TwrParameter: 'Parameter 1', MinionOperator: 'A(1)', KyeberCrystalValue: 3},
          {PoNumber: '03466716228', ActualUnitPrice: 15, SomeOtherPriceCoef: 32, CompletelyMadeUpNumber: 54, ActualPriceCoef: 32, RandomComparator: 'KPI Comparator', Ppv: 14.3, VatIncluded: 'True', OrbitCost: 52.8, TwrParameter: 'Parameter 1', MinionOperator: 'A(2)', KyeberCrystalValue: 10},
          {PoNumber: '03937645442', ActualUnitPrice: 44, SomeOtherPriceCoef: 144, CompletelyMadeUpNumber: 89, ActualPriceCoef: 80, RandomComparator: 'Cargo', Ppv: 11.10, VatIncluded: 'True', OrbitCost: 38.5, TwrParameter: 'Parameter-2', MinionOperator: 'F2(1)', KyeberCrystalValue: 5},
          {PoNumber: '75949448492', ActualUnitPrice: 54, SomeOtherPriceCoef: 137, CompletelyMadeUpNumber: 7, ActualPriceCoef: 5, RandomComparator: 'Randomizer', Ppv: 22, VatIncluded: 'False', OrbitCost: 28.55, TwrParameter: 'Parameter-3', MinionOperator: 'Fi(1)', KyeberCrystalValue: 4},
          {PoNumber: '63432143229', ActualUnitPrice: 16, SomeOtherPriceCoef: 26, CompletelyMadeUpNumber: 34, ActualPriceCoef: 30, RandomComparator: 'Default', Ppv: 14.3, VatIncluded: 'True', OrbitCost: 43.3, TwrParameter: 'Parameter 2', MinionOperator: 'A(1)', KyeberCrystalValue: 0},
          {PoNumber: '54987699492', ActualUnitPrice: 43, SomeOtherPriceCoef: 114, CompletelyMadeUpNumber: 49, ActualPriceCoef: 80, RandomComparator: 'Cargo', Ppv: 12.87, VatIncluded: 'False', OrbitCost: 38.5, TwrParameter: 'Parameter-2', MinionOperator: 'FA2(1)', KyeberCrystalValue: 1},
          {PoNumber: '11929215229', ActualUnitPrice: 36.55, SomeOtherPriceCoef: 46, CompletelyMadeUpNumber: 37, ActualPriceCoef: 31, RandomComparator: 'KPI Comparator', Ppv: 32, VatIncluded: 'True', OrbitCost: 83.3, TwrParameter: 'Parameter 1', MinionOperator: 'FA(2)', KyeberCrystalValue: 31},
          {PoNumber: '00199448492', ActualUnitPrice: 50.6, SomeOtherPriceCoef: 47, CompletelyMadeUpNumber: 17, ActualPriceCoef: 18, RandomComparator: 'Randomizer 2', Ppv: 44, VatIncluded: 'False', OrbitCost: 18.55, TwrParameter: 'Parameter-3', MinionOperator: 'F2(1)', KyeberCrystalValue: 0},
          {PoNumber: '24343315229', ActualUnitPrice: 36, SomeOtherPriceCoef: 76, CompletelyMadeUpNumber: 7, ActualPriceCoef: 30, RandomComparator: 'Random', Ppv: 12.3, VatIncluded: 'True', OrbitCost: 43.3, TwrParameter: 'Parameter 1', MinionOperator: 'A(1)', KyeberCrystalValue: 3},
          {PoNumber: '14347548492', ActualUnitPrice: 83, SomeOtherPriceCoef: 97, CompletelyMadeUpNumber: 9, ActualPriceCoef: 91, RandomComparator: 'Randomizer', Ppv: 32.6, VatIncluded: 'False', OrbitCost: 78.55, TwrParameter: 'Parameter-2', MinionOperator: 'FA(1)', KyeberCrystalValue: 1},
          {PoNumber: '95744215229', ActualUnitPrice: 26.5, SomeOtherPriceCoef: 56, CompletelyMadeUpNumber: 17, ActualPriceCoef: 20, RandomComparator: 'Default', Ppv: 2.3, VatIncluded: 'False', OrbitCost: 43.3, TwrParameter: 'Parameter 1', MinionOperator: 'A(1)', KyeberCrystalValue: 13},
          {PoNumber: '24449448492', ActualUnitPrice: 74, SomeOtherPriceCoef: 137, CompletelyMadeUpNumber: 7, ActualPriceCoef: 5, RandomComparator: 'Random', Ppv: 22, VatIncluded: 'True', OrbitCost: 28.55, TwrParameter: 'Parameter-2', MinionOperator: 'Fi(2)', KyeberCrystalValue: 5},
          {PoNumber: '93387695492', ActualUnitPrice: 33, SomeOtherPriceCoef: 80, CompletelyMadeUpNumber: 46, ActualPriceCoef: 80, RandomComparator: 'Cargo', Ppv: 12.7, VatIncluded: 'False', OrbitCost: 35.25, TwrParameter: 'Parameter 1', MinionOperator: 'FA2(1)', KyeberCrystalValue: 1},
          {PoNumber: '72948404492', ActualUnitPrice: 51, SomeOtherPriceCoef: 139, CompletelyMadeUpNumber: 7, ActualPriceCoef: 5, RandomComparator: 'Randomizer', Ppv: 22, VatIncluded: 'False', OrbitCost: 25.5, TwrParameter: 'Parameter-2', MinionOperator: 'Fi(1)', KyeberCrystalValue: 0},
          {PoNumber: '65343655229', ActualUnitPrice: 26, SomeOtherPriceCoef: 77, CompletelyMadeUpNumber: 4, ActualPriceCoef: 20, RandomComparator: 'Default', Ppv: 12.3, VatIncluded: 'True', OrbitCost: 23.5, TwrParameter: 'Parameter 1', MinionOperator: 'AF(1)', KyeberCrystalValue: 2},
          {PoNumber: '63466766229', ActualUnitPrice: 17, SomeOtherPriceCoef: 22, CompletelyMadeUpNumber: 44, ActualPriceCoef: 30, RandomComparator: 'KPI Comparator', Ppv: 14.3, VatIncluded: 'True', OrbitCost: 53.3, TwrParameter: 'Parameter 1', MinionOperator: 'A(1)', KyeberCrystalValue: 0},
          {PoNumber: '54987645444', ActualUnitPrice: 43, SomeOtherPriceCoef: 124, CompletelyMadeUpNumber: 69, ActualPriceCoef: 80, RandomComparator: 'Cargo', Ppv: 12.87, VatIncluded: 'True', OrbitCost: 38.5, TwrParameter: 'Parameter-2', MinionOperator: 'FA2(1)', KyeberCrystalValue: 2},
          {PoNumber: '21929544422', ActualUnitPrice: 6.55, SomeOtherPriceCoef: 26, CompletelyMadeUpNumber: 27, ActualPriceCoef: 31, RandomComparator: 'KPI Comparator', Ppv: 22, VatIncluded: 'True', OrbitCost: 53.3, TwrParameter: 'Parameter 1', MinionOperator: 'FA(2)', KyeberCrystalValue: 1},
          {PoNumber: '04199324492', ActualUnitPrice: 40.6, SomeOtherPriceCoef: 67, CompletelyMadeUpNumber: 37, ActualPriceCoef: 18, RandomComparator: 'Randomizer 2', Ppv: 24, VatIncluded: 'False', OrbitCost: 13.52, TwrParameter: 'Parameter 1', MinionOperator: 'F2(1)', KyeberCrystalValue: 0}
        ];

        this.content = {
            views: {
                ActualUnitPrice: {index: 0, columns: ['PoNumber', 'ActualUnitPrice', 'SomeOtherPriceCoef', 'CompletelyMadeUpNumber']},
                Rr2UnitPrice: {index: 1, columns: ['PoNumber', 'ActualUnitPrice', 'VatIncluded', 'OrbitCost', 'InternationalPrice']},
                PpvAtBuRate: {index: 2, columns: ['PoNumber', 'Ppv', 'RandomComparator']},
                Custom: {index: 3, columns: []}
            }
        };
        this.state = {
            selectedViewIndex: 0,
            selectedData: this.utils.selectDataPerView.call(this, 0)
        };
   }
    handleTabChange = (index) => {
        this.refs.grid.resetSort();
        this.setState({selectedViewIndex: index,
            selectedData: this.utils.selectDataPerView.call(this, index)
        });
      };

    handleSortChange = (column, flipDirection) => {
        this.setState({ selectedData: this.state.selectedData.sort((a, b) => {
            const direction = flipDirection ? -1 : 1;
            let aA = a[column], bB = b[column];
            if (typeof (a[column]) === 'string' && typeof (b[column]) === 'string') {
                aA = aA.toLowerCase();
                bB = bB.toLowerCase();
            }
             if (aA > bB) {
                 return direction * -1;
             }
             if (aA < bB) {
                 return direction * 1;
             }
             return 0;
        })});
        this.forceUpdate();
    };

    handleCustomViewChange = (columns) => {
        this.content.views.Custom.columns = columns;
        if (this.state.selectedViewIndex === 3) { //3 is the index of custom tab
            this.handleTabChange(this.state.selectedViewIndex);
            this.refs.grid.resetSort();
            this.forceUpdate();
        }

    }

   utils = {
       selectDataPerView (viewIndex) {
           const columns = this.content.views[Object.keys(this.content.views).find((item) => {
              return this.content.views[item].index === viewIndex;
          })].columns;

           return this.data.map((item) => {
               return Utils.reduceObjectProperties(item, columns);
           });
       }

   };

   render () {
       const tabs = Object.keys(this.content.views).map((item, index) => {
           return <Tab label={Utils.separateCamelWords(item)} key={index} />
       });

       return <div className={style.GridView}>
                <div className={style.headerContainer}>
                       <Tabs index={this.state.selectedViewIndex} onChange={this.handleTabChange} className={style.tabs} >
                            {tabs}
                       </Tabs>
                   <CustomMenu columns={this.data[0]} handleCustomViewChange={this.handleCustomViewChange.bind(this)}/>
               </div>


                <Grid viewData = {this.state.selectedData} handleSortChange={this.handleSortChange.bind(this)} ref="grid"/>
    </div>;
   }
}

GridViewComponent.propTypes = {

};

export default GridViewComponent;
