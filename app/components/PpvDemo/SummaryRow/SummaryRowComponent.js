import React from 'react';
import style from './SummaryRowComponent.scss';

class SummaryRowComponent extends React.Component {

   constructor (props) {
      super(props);
      if (!props.item) {
          props.item = {};
      }

      this.content = {

      };
   }

   render () {
       const widthColumn = (100 / (Object.keys(this.props.item).length)) + '%';
       const row = Object.keys(this.props.item).map((key, index) => {
           return <div key={index} className={style.rowCell} key={index} style={{ width: widthColumn}}>{this.props.item[key]}</div>;
        });
       return <div className={style.Row}>
                 {row}
            </div>;
   }
 }

 SummaryRowComponent.propTypes = {
    item: React.PropTypes.object
 };

 export default SummaryRowComponent;
