import React from 'react';
import FontIcon from 'react-toolbox/lib/font_icon';
import { StickyContainer, Sticky } from 'react-sticky';
import {Utils} from '../../../utils/utils.js';
import style from './HeaderRowComponent.scss';

class HeaderRowComponent extends React.Component {

   constructor (props) {
      super(props);
      let startColumn = '';
      if (!props.item) {
          props.item = {};
      }
      if (Object.keys(this.props.item).length > 0) {
          //startColumn = Object.keys(this.props.item)[0];
      }
      this.content = {
          sortColumn: startColumn,
          directionAsc: true
      };
   }
   handleSortChange = (column) => {
       this.content.directionAsc = column !== this.content.sortColumn || !this.content.directionAsc;
       this.props.handleSortChange(column, this.content.directionAsc);
       this.content.sortColumn = column;
   };
   getSortIcon = (column) => {
       if (column === this.content.sortColumn) {
           if (this.content.directionAsc) {
               return 'arrow_downward';
           }
           return 'arrow_upward';
       }
       return '';
   }
   resetSort = () => {
       this.content.sortColumn = '';
   }

   render () {
       const widthColumn = (100 / (Object.keys(this.props.item).length)) + '%';
       const row = Object.keys(this.props.item).map((key, index) => {
           return <div key={index} className={style.rowCell} style={{ width: widthColumn}} onClick={() => {this.handleSortChange(key);}}> <FontIcon value={this.getSortIcon(key)} className={style.sort} /> {Utils.separateCamelWords(key)} </div>;
        });
       return <div className={style.HeaderRow}>
                 {row}
            </div>;
   }
}

HeaderRowComponent.propTypes = {
    handleSortChange: React.PropTypes.func,
    item: React.PropTypes.object
};

export default HeaderRowComponent;
