import React from 'react';
import { AppBar, Navigation} from 'react-toolbox';
import { Layout, NavDrawer, Panel } from 'react-toolbox';
import GridView from './GridView/GridViewComponent.js';
import style from './PpvDemoComponent.scss';
class PpvDemo extends React.Component {

  static contextTypes = {
      params: React.PropTypes.object
  }

  constructor () {
      super();
  }

  state = {
      drawerActive: false,
      drawerPinned: false,
      sidebarPinned: false
  };

  toggleDrawerActive = () => {
      this.setState({ drawerActive: !this.state.drawerActive });
  };

  toggleDrawerPinned = () => {
      this.setState({ drawerPinned: !this.state.drawerPinned });
  }

  toggleSidebar = () => {
      this.setState({ sidebarPinned: !this.state.sidebarPinned });
  };

  render () {
      return (
          <Layout className={style.Ppv}>
              <NavDrawer active = {this.state.drawerActive}
                  pinned = {this.state.drawerPinned} permanentAt='xxxl'
                  onOverlayClick = {this.toggleDrawerActive}>
                  <p>
                      Additional menu, options etc. go here.
                  </p>
              </NavDrawer>
              <Panel>
                      <AppBar rightIcon='menu' onRightIconClick={this.toggleDrawerActive} leftIcon='arrow_back' >
                          <Navigation type='horizontal'>
                              <span className={style.title}>Purchase Order Commander</span>
                          </Navigation>
                      </AppBar>

                  <div className={style.content}>
                      <GridView />
                  </div>
              </Panel>
              <div className={style.footer}></div>
          </Layout>
          );
      }

}

export default PpvDemo;
