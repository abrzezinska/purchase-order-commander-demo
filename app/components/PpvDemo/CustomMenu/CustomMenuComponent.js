import React from 'react';
import {IconMenu, MenuItem, MenuDivider } from 'react-toolbox/lib/menu';
import Checkbox from 'react-toolbox/lib/checkbox';
import {Button} from 'react-toolbox/lib/button';
import {Utils} from '../../../utils/utils.js';
import style from './CustomMenuComponent.scss';

//var injectTapEventPlugin = require("react-tap-event-plugin");
//injectTapEventPlugin();

class CustomMenuComponent extends React.Component {

  constructor (props) {
        super();
        this.content = {
            columns: {}, //Object.keys(props.columns),
            selectedCounter: 0,
            MAX_SELECTED: 6,
            selectedColumns: []
        };
        /*for (const prop in this.content.columns) {
             if (props.columns.hasOwnProperty(prop)) {
                 this.content.columns[prop] = false;
             }
         }*/
         Object.keys(props.columns).forEach((prop) => {
             this.content.columns[prop] = false;
         });
        this.state = {checked: this.content.columns};
  }
  handleSelectColumn = (field, value) => {
      if (value) {
          if (this.content.selectedCounter < this.content.MAX_SELECTED) {
              this.setState({checked: Object.assign(this.state.checked, {[field]: value})});
              ++this.content.selectedCounter;
          }
      } else {
          this.setState({checked: Object.assign(this.state.checked, {[field]: value})});
          --this.content.selectedCounter;
      }
  };
  handleCustomViewChange = () => {
      //take selected columns
      this.content.selectedColumns = Object.keys(this.state.checked).filter((name) => {
          return this.state.checked[name];
      });
      this.content.selectedCounter = this.content.selectedColumns.length;
      this.props.handleCustomViewChange(this.content.selectedColumns);

      //reset to applied columns
      Object.keys(this.props.columns).forEach((prop) => {
          this.content.columns[prop] = (this.content.selectedColumns.indexOf(prop) !== -1);
      });
     this.setState({checked: this.content.columns});
  }

  handleShow = () => {
      Object.keys(this.props.columns).forEach((prop) => {
          this.content.columns[prop] = (this.content.selectedColumns.indexOf(prop) !== -1);
      });
      this.content.selectedCounter = this.content.selectedColumns.length;
      this.setState({checked: this.content.columns});
  };


  render () {
      let column1 = [], column2 = [];
      if (Object.keys(this.props.columns).length > 0) {
          const columns = Object.keys(this.props.columns).map((name) => {
              return <MenuItem value={name} className={style.caption} key={name}>
                       <Checkbox
                           checked={this.state.checked[name]}
                           className={style.checkbox}
                           onChange={this.handleSelectColumn.bind(this, name)}
                           label={Utils.separateCamelWords(name)}

                         />
                   </MenuItem>;
          });
          const half = Math.ceil(Object.keys(this.props.columns).length / 2);
          column1 = columns.slice(0, half);
          column2 = columns.slice(half + 1);
      }

      return (
              <IconMenu icon='more_horiz' position='topRight' menuRipple className={style.customMenu} onShow={this.handleShow.bind(this)}>
                    <div className={style.header}>Choose custom columns (max. 6)</div>
                    <div className={style.columns}>
                        <span className={style.column}>
                            {column1}
                        </span>
                        <span className={style.column}>
                            {column2}
                        </span>
                    </div>
                    <MenuItem value='Apply' caption='Apply' onClick={this.handleCustomViewChange.bind(this)} className={style.footer}/>
              </IconMenu>
      );
  }

}

CustomMenuComponent.propTypes = {
    columns: React.PropTypes.object,
    handleCustomViewChange: React.PropTypes.func,
    handleSortChange: React.PropTypes.func
};
export default CustomMenuComponent;
