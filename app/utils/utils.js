
class Utils {

    static separateCamelWords (word) {
       return word
         // insert a space before all caps
         .replace(/([A-Z])/g, ' $1')
         // uppercase the first character
         .replace(/^./, function (str) {
              return str.toUpperCase();
          });
    }
    static reduceObjectProperties (item, reducedProperites) {
        const destObj = {};
        for (const key in item) {
            if (item.hasOwnProperty(key) && reducedProperites.indexOf(key) !== -1) {
                destObj[key] = item[key];
            }
        }
       return destObj;
    }
}

export { Utils };
