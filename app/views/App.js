import React from 'react';
//import { injectTapEventPlugin }  from 'react-tap-event-plugin';
//injectTapEventPlugin();

class App extends React.Component {

  static propTypes = {
      children: React.PropTypes.any,
      params: React.PropTypes.object
  }

  static childContextTypes = {
      params: React.PropTypes.object
  }

  constructor () {
      super();
  }

  state = {}

  getChildContext () {
      return { params: this.props.params };
  }

  render () {
      return <div>
        {this.props.children}
      </div>;
  }
}

export default App;
