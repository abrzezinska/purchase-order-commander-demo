import React from 'react';
import { Link } from 'react-router';

export default function Home () {
  return <div>
    <h1>Home</h1>
    <h4>Environment: <em>{self.Environment.env.name}</em></h4>
    <ul>
    <li>
      <Link to="/ppv-demo">PPV Demo</Link>
    </li>
      <li>
        <Link to="/material-design">Material Design Demo Page</Link>
      </li>
      <li>
        <Link to="/material-design-123">Material Design Demo Page + route param test</Link>
      </li>
    </ul>
  </div>;
}
