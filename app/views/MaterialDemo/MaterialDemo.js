import React from 'react';
import Autocomplete from '../../components/MaterialDemo/autocomplete';
import Avatar from '../../components/MaterialDemo/avatar';
import FontIcon from '../../components/MaterialDemo/font_icon';
import Button from '../../components/MaterialDemo/button';
import Card from '../../components/MaterialDemo/card';
import CardIntro from '../../components/MaterialDemo/card-intro';
import Checkbox from '../../components/MaterialDemo/checkbox';
import Chip from '../../components/MaterialDemo/chip';
import Dialog from '../../components/MaterialDemo/dialog';
import Drawer from '../../components/MaterialDemo/drawer';
import Dropdown from '../../components/MaterialDemo/dropdown';
import IconMenu from '../../components/MaterialDemo/icon_menu';
import Input from '../../components/MaterialDemo/input';
import Layout from '../../components/MaterialDemo/layout';
import List from '../../components/MaterialDemo/list';
import Menu from '../../components/MaterialDemo/menu';
import Pickers from '../../components/MaterialDemo/pickers';
import Progress from '../../components/MaterialDemo/progress';
import Radio from '../../components/MaterialDemo/radio';
import Slider from '../../components/MaterialDemo/slider';
import Snackbar from '../../components/MaterialDemo/snackbar';
import Switch from '../../components/MaterialDemo/switch';
import Table from '../../components/MaterialDemo/table';
import Tabs from '../../components/MaterialDemo/tabs';
import Tooltip from '../../components/MaterialDemo/tooltip';
import SampleComponent from '../../components/SampleComponent/SampleComponent.js';
import app from '../../components/MaterialDemo/style';
export default function Material () {
  return <div>
    <h1>Material Design Demo Page</h1>
    <SampleComponent />
    <div className={app.materialList}>
      <CardIntro />
      <Autocomplete />
      <Avatar />
      <FontIcon />
      <Card />
      <Button />
      <Checkbox />
      <Chip />
      <Dialog />
      <Drawer />
      <Dropdown />
      <IconMenu />
      <Input />
      <Layout />
      <List />
      <Menu />
      <Pickers />
      {<Progress />}
      <Radio />
      <Slider />
      <Snackbar />
      <Switch />
      <Table />
      <Tabs />
      <Tooltip />
    </div>
  </div>;
}
