import 'react-toolbox/lib/commons.scss';
import 'babel-polyfill';
import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import RootComponent from './components/Root/Root.js';

document.ontouchmove = function(event){
    event.preventDefault();
}

ReactDOM.render(<RootComponent />, document.getElementById('rb-container'));
