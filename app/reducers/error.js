import { ERROR } from './constants.js';

// ACTIONS

export const displayError = (options) => {
    return {
        type: ERROR,
        options
    };
};


// REDUCEERS

const initialState = {
    show: false,
    message: 'Default Message',
    random: 'X'
};

export function error (state = initialState, action = {}) {
    switch (action.type) {
        case ERROR:
            return {
                show: action.options.show,
                message: action.options.message,
                random: Math.random()
            };
        default:
            return state;
    }
}
